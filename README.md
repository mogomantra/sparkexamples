# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

### Conceptual Questions
Q1 What is the difference between RDD, DataSet and DataFrame?

Q2 What is the difference between SparkContext and SparkSession?

Q3 What are Accumulators and BroadCast Variables and how are they used?

Q4 What is Spark Architecture and program components?
	Spark Driver, Cluster Manager, Worker, Executor, task

Q5 What is the difference between Transformation and Action?

Q6 What are various cluster managers which Spark can use?

Q7 Example Code - Explain this code and issue with it
	val spark = SparkSession.builder()
				.master("Master URL")
				.appName("Sample MoGo Tutorial App")
				.getOrCreate()
	val rdd = spark.sparkContext.textFile("src/main/resources/people.json")
	rdd.collect().forEach(print)
Q8 What are the encoders in Spark and how are they used?

Q9 Which format options Spark support ? parquet, json, csv, orc, jdbc, libsvm

Q10 What the Save Mode -> SaveMode.ErrorIfExists, SparkMode.Ignore, SparkMode.Overwrite, SparkMode.Append

Q11 How table partitioning works in Spark
Table is saved to path with partition keys. 
Full Path - /path/to/table/gender=male/country=us. In this path /path/to/table is base path and gender=male and country=us are the partition columns.
column type is auto inference. Currently supported numeric and string. by default spark.sql.sources.partitionColumnTypeInference.enabled = true.
When /path/to/table/gender=male/country=us is supplied for spark.read.load function or spark.read.parquet function, gender=male is treated as part of path not the partition column.

Q12 What is schema merging in spark?
Spark allows us to add additional columns in future. This is table extensibility without breaking the table structure. This is good concept but very difficult to use in practice. By default it turned off.

Q13 How do you integrate Hive with Spark?
Spark can be used to read data from hive tables. Spark can be used as SQL Engine.
Put hdfs-site.xml, hive-site.xml and core-site.xml files in conf dir and putting all the Hive jars in Spark Lib directory.

Q14 How do you setup the Cluster Manager for Spark?
Q15 What are encoders and how are they used in Spark?


 
	
