package com.mogo.app.learn.spark.sql

import org.apache.spark.sql.SparkSession


/**
 * This Example illustrate how a schema is attached to RDD via inferring 
 */
object SchemaInferenceExample extends App{
  
  val spark = SparkSession.builder
      .master("local")
      .appName("MoGo Schema Inference Example App")
          .getOrCreate()
  val path = "src/main/resources/people.txt"
  val textFile = spark.sparkContext.textFile(path)
  
  
  case class Person(name:String, age:Long)
  
  /*
   * split each word by , and assign each record a name and type
   * This is where Driver submits the code to workers to perform actual task when an action is called.
   */
  val people = textFile.map(_.split(","))
    .map(attributes => Person(attributes(0), attributes(1).trim.toLong))
    
  /**
   * covert people RDD to DataFrame
   * import spark.implicits._  
   */
  import spark.implicits._  
  val peopleDF = people.toDF()
  peopleDF.show()
  peopleDF.printSchema()
  
  /**
   * Create a temp View and use Spark SQL for queries
   */
  
 
  peopleDF.createOrReplaceTempView("people")
  val teenDF = spark.sql("select * from people WHERE age BETWEEN 13 AND 19") 
  
  //get the data by fieldNumber
  teenDF.map(teenager => "Name :"+teenager(0)).show()
  
  //get the data by field name
  teenDF.map(teenager => "Name :"+teenager.getAs("name")).show()
  
  implicit val mapEncoder = org.apache.spark.sql.Encoders.kryo[Map[String, Any]]
  teenDF.map(teenager => teenager.getValuesMap[Any](List("name", "age"))).collect().foreach(print)
          
   spark.close(); 
}