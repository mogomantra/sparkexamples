package com.mogo.app.learn.spark.sql

import org.apache.spark.sql.SparkSession

/**
 * This example illustrate schemamerge
 */
object SchemaMergeExample extends App{
  
  val spark = SparkSession.builder()
    .master("local")
    .appName("Merge Schema Example")
    .getOrCreate()
  
    import spark.implicits._
  val p1 = spark.sparkContext.makeRDD(1 to 10).map(a => (a, a*a)).toDF("value", "squre")
  p1.write.parquet("data/test_table/key=1")
  
  val p2 = spark.sparkContext.makeRDD(1 to 10).map( a => (a, a*a*a)).toDF("value", "cube")
  p2.write.parquet("data/test_table/key=2")
  
  val mergeSchemaDF = spark.read.option("mergeSchema", "true").parquet("data/test_table")
  mergeSchemaDF.printSchema()
  
}