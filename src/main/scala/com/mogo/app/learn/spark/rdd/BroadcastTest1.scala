package com.mogo.app.learn.spark.rdd

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext

object BroadcastTest1 {
  
  def main(args: Array[String]) {
    
    val bcName = if (args.length > 2) args(2) else "Http";
    val blockSize = if(args.length > 3) args(3) else "4096";
    
    val sparkConf = new SparkConf().setMaster("local[4]")
      .setAppName("Broadcast Test")
      .set("spark.broadcast.factory", s"org.apache.spark.broadcast.${bcName}BroadCastFactory")
      .set("spark.broadcast.bloackSize", blockSize);
    val sc = new SparkContext(sparkConf);
    
    val slices = if (args.length > 0) args(0).toInt else 2
    val num = if (args.length >1 ) args(1).toInt else 1000000
    
    val arr1 = (0 until num).toArray
    
    for( i <- 0 until 3) {
      println("Iteration " + i)
      println ("==============")
      val startTime = System.nanoTime
      val barr1 = sc.broadcast(arr1)
      val observedSizes = sc.parallelize(1 to 10, slices).map(_ => barr1.value.size)
      observedSizes.collect().foreach ( x => println(x) )
      println("Iteration %d took %.0f millisecond".format(i, (System.nanoTime() - startTime) / 1E6))
      
    }
    sc.stop()
    
  }
  
}