package com.mogo.app.learn.spark.ml

/**
 * This example will explain how to perform text pre processing 
 * Example - SparkText -  http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0162721
 * Text Preprocessing involve following steps
 * 0. Language translation
 * 0. Parsed pre categorized abstracts or full-text articles into sentences.
 * 1. Replacing special symbols and punctuation marks with blank space 
 * 		(We then replaced special characters, such as quotation marks and other punctuation marks, with blank spaces)
 * 2. case normalizaton ( marked all sentences in a lower case format to provide normalized statements)
 * 3. removing duplicate characters, rare words and user-defined stop words. 
 * 		(Afterwards, we parsed sentences into individual words (tokens). Rare words and user-defined stop-words were removed)
 * 4. Word stemming (and the Porter Stemmer algorithm [25,26] was then used to stem all words.)
 */

object TextProcessing {
  
}