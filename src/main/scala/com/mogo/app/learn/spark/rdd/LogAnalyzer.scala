package com.mogo.app.learn.sparkexamples

import org.apache.spark.sql.SparkSession


/**
 * This example reads the log file and analyze the logs
 */

object LogAnalyzer {

  def main(args: Array[String]): Unit = {

    //currently the file name is hardcoded but use this as input parameter
    val fileName = "apache01.log"

    //create the spark driver
    val spark = SparkSession.builder()
      .master("local[4]")
      .appName("Log Analyzer App")
      .getOrCreate();

    //retrieve spark session
    val sc = spark.sparkContext

    //load the log file
    val logRDD = sc.textFile("src/main/resources/apache03.log", 2)

    //print input file
    //logRDD.collect().foreach { x => println(x) }
    val wordCount = logRDD
      .flatMap { x => x.split(" ") }
      .flatMap { x => x.split("\t") }
      .filter { x => x.nonEmpty }
      .map { x => (x, 1) }
      .countByKey()
      .values.sum

    //spiltedRDD.collect().foreach { x => println(x) }
    println(s"%d words in the file".format(wordCount))
      
  val exampleApacheLogs = List(
    """10.10.10.10 - "FRED" [18/Jan/2013:17:56:07 +1100] "GET http://images.com/2013/Generic.jpg
      | HTTP/1.1" 304 315 "http://referall.com/" "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;
      | GTB7.4; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30; .NET CLR 3.0.04506.648; .NET CLR
      | 3.5.21022; .NET CLR 3.0.4506.2152; .NET CLR 1.0.3705; .NET CLR 1.1.4322; .NET CLR
      | 3.5.30729; Release=ARP)" "UD-1" - "image/jpeg" "whatever" 0.350 "-" - "" 265 923 934 ""
      | 62.24.11.25 images.com 1358492167 - Whatup""".stripMargin.lines.mkString,
    """10.10.10.10 - "FRED" [18/Jan/2013:18:02:37 +1100] "GET http://images.com/2013/Generic.jpg
      | HTTP/1.1" 304 306 "http:/referall.com" "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;
      | GTB7.4; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30; .NET CLR 3.0.04506.648; .NET CLR
      | 3.5.21022; .NET CLR 3.0.4506.2152; .NET CLR 1.0.3705; .NET CLR 1.1.4322; .NET CLR
      | 3.5.30729; Release=ARP)" "UD-1" - "image/jpeg" "whatever" 0.352 "-" - "" 256 977 988 ""
      | 0 73.23.2.15 images.com 1358492557 - Whatup""".stripMargin.lines.mkString
  )


    val apacheLogRegex =
      """^([\d.]+) (\S+) (\S+) \[([\w\d:/]+\s[+\-]\d{4})\] "(.+?)" (\d{3}) ([\d\-]+) "([^"]+)" "([^"]+)".*""".r

     /** Tracks the total query count and number of aggregate bytes for a particular group. */
    class Stats(val count: Int, val numBytes: Int) extends Serializable {
      def merge(other: Stats): Stats = new Stats(count + other.count, numBytes + other.numBytes)
      override def toString: String = "bytes=%s\tn=%s".format(numBytes, count)
    }
      
    def extractKey(line: String): (String, String, String) = {
      apacheLogRegex.findFirstIn(line) match {
        case Some(apacheLogRegex(ip, _, user, dateTime, query, status, bytes, referer, ua)) =>
          if (user != "\"-\"") (ip, user, query)
          else (null, null, null)
        case _ => (null, null, null)
      }
    }

    def extractStats(line: String): Stats = {
      apacheLogRegex.findFirstIn(line) match {
        case Some(apacheLogRegex(ip, _, user, dateTime, query, status, bytes, referer, ua)) =>
          new Stats(1, bytes.toInt)
        case _ => new Stats(1, 0)
      }
    }
   
   val apacheLogRDD = sc.parallelize(exampleApacheLogs, 2) 
   val datasetRDD = apacheLogRDD.map { line => (extractKey(line), extractStats(line))}
   datasetRDD.collect().foreach(println)
   val datasetRDD2 = datasetRDD.reduceByKey((a,b) => a.merge(b))
   println("Mohan Goyal")
   datasetRDD2.collect().foreach{case (user, query) => println("mohan %s\t%s".format(user, query))}

    spark.stop

  }
}