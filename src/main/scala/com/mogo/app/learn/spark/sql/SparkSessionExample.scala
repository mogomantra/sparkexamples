package com.mogo.app.learn.spark.sql

import org.apache.spark.sql.SparkSession

object SparkSessionExample {
  
  def main(args: Array[String]): Unit = {
    val sparkSession = SparkSession.builder.master("local").appName("MoGo App").getOrCreate(); 
    val df = sparkSession.read.option("header","true").csv("src/main/resources/sales.csv");
    df.printSchema();
    	val rdd = sparkSession.sparkContext.textFile("src/main/resources/people.json")
	    rdd.collect().map(print)
  }
}