package com.mogo.app.learn.spark.sql

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.SaveMode

/**
 * This Example illustrates how to use various data sources
 */
object DataSourceExample extends App{
  
  val spark  = SparkSession.builder
    .master("local")
    .appName("Spark SQL DataSource Examples")
    .getOrCreate();
  
  /**
   * Default format for Spark is parquet which is columnnar format
   * Generic Load and Save functions
   */
  
  val usersDF = spark.read.load("src/main/resources/users.parquet")

  usersDF.select("name", "favorite_color").write.mode(SaveMode.Overwrite)save("nameAndFavoriteColor.parquet")
  
  //manually specifying options
  //supported formats json, parquet, jdbc, orc, libsvm, csv, text
  val peopleDF = spark.read.format("json").load("src/main/resources/people.json")
  //this is same as
  val peopleDF1 = spark.read.json("src/main/resources/people.json")
  peopleDF.select("name", "age").write.format("parquet").mode(SaveMode.Overwrite).save("namesAndAges.parquet")
  
  
  //saving to persistence table
  import spark.implicits._
  
  val peopleDF2 = spark.read.json("src/main/resources/people.json")
  
  peopleDF2.write.mode(SaveMode.Overwrite).parquet("people.parquet")
  
  //read the parquet file again
  
  val peopleDF3 = spark.read.parquet("people.parquet")
  
  //create temp view
  peopleDF3.createOrReplaceTempView("people");
  val resultDF = spark.sql("select * from people where age between 13 and 19")
  resultDF.map(attributes => "Name : "+ attributes(0)).show()
  
  
  
  
  
  
}