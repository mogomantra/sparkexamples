package com.mogo.app.learn.spark.sql

import org.apache.spark.sql.SparkSession

/**
 * This explains how to use hive tables using Spark. 
 */

object HiveExample extends App{
  val warehouseLocation = "mogodb"
  val spark = SparkSession.builder()
    .master("local")
    .appName("Hive Example App")
    .config("spark.sql.warehouse.dir", warehouseLocation)
    .enableHiveSupport()
    .getOrCreate()
  
  import spark.implicits._
  import spark.sql
  
  sql("CREATE TABLE IF NOT EXISTS MOGO_TBL (key INT, value STRING)")
  sql("LOAD DATA LOCAL INPATH 'src/main/resources/kv1.txt' INTO MOGO_TBL")
  
  sql("select * from MOGO_TBL").show()
  
  sql("SELECT COUNT(*) FROM MOGO_TBL").show()
  
  
  spark.close()
  
    
  
}