package com.mogo.app.learn.spark.sql

import org.apache.spark.sql.SparkSession

object DataFrameExample {
  def main(args: Array[String]): Unit = {
    
    //This is the new way of creating SparkSession.
    //spark.driver.extraJavaOptions is required for driver logs when we submit the job to spark cluster.
    //Driver may be running @ one of cluster node (cluster mode) or client machine (client mode)
    val spark = SparkSession.builder()
      .master("local")
      .appName("DataFrameExample")
      .config("spark.some.config.option", "some-value")
      .config("spark.driver.extraJavaOptions", "-Dlog4j.configuration=log4j.properties") 
      .getOrCreate()
      
   /*
    * reading a JSON file
    * Spark has loaded the JSON file and schema is assigned automatically by introspection.
    */
   val df = spark.read.json("src/main/resources/people.json")
   //val ss = spark.sparkContext.textFile("src/main/resources/people.json", 10)
   //ss.
   
   /*
    * want to see the data.
    * this will fire a Spark Job to read the file and get the data back to Driver.
    * Driver will show the data. In practice we should never run this command as 
    * it will try to bring all the data to driver which can crash the driver process.
    */
   df.show()
   
   /*
    * print the schema of the DF
    */
   df.printSchema() 
   
   /* 
    * Select the column which you want to choose
    */
   df.select("name").show()
   
   // This import is needed to use the $-notation
    import spark.implicits._
   
   df.select($"name", $"age"+1).show()
   df.filter($"age" > 21).show()
   
   // Count people by age
   df.groupBy("age").count().show()
   
   
   //Register DataFrame as a SQL temporary view.
   df.createOrReplaceTempView("people")
   val sqlDF = spark.sql("select * from people")
   sqlDF.show()
   
   //creating global view 
   df.createGlobalTempView("people")
   val gSqlDF = spark.sql("select * from global_temp.people")
   gSqlDF.show()
   
   //access global temp view from new Spark Session
   val newSession = spark.newSession
   val gNewSqlDF = newSession.sql("select * from global_temp.people")
   gNewSqlDF.show()
   
   spark.stop()
    
    
    
  }
  
  
  
  
  
}