package com.mogo.app.learn.spark.sql

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.Row
import org.apache.spark.sql.types._
  
object SchemaProgrammaticExample extends App{
  val spark = SparkSession.builder
    .master("local")
    .appName("Programmatically Specifying Schema Example")
    .getOrCreate();
  
  val path = "src/main/resources/people.txt"
  //read one line at a time
  val peopleTextRDD = spark.sparkContext.textFile(path)
  
  //split the line with comma
  val peopleRDD = peopleTextRDD.map(_.split(","))
  val rowRDD = peopleRDD.map(attributes => Row(attributes(0), attributes(1)))
  
  //create Schema Programmatically
  val schemaStr = "name age"
  val fields = schemaStr.split(" ")
    .map(fieldName => StructField(fieldName, StringType, nullable = true))
  val schema = StructType(fields)
  
  //apply Schema to RDD
  val peopleDF = spark.createDataFrame(rowRDD, schema)
  
  peopleDF.printSchema()
  
  peopleDF.createOrReplaceTempView("people")
  
  val results = spark.sql("select name from people")
  
  import spark.implicits._ 
  
  results.map(attributes => "Name : " + attributes(0)).show()
 
  
  
  
  
}