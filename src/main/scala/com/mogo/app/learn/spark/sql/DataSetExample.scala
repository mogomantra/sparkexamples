package com.mogo.app.learn.spark.sql

import org.apache.spark.sql.SparkSession

/**
 * This Example explains Spark DataSet Concepts
 * Note that this example extends from App 
 * which has main function which calls the code written here.
 */
object DataSetExample extends App{
  
  //Create Spark Session
  val spark = SparkSession.builder()
    .master("local")
    .appName("MoGo Spark DataSet Example")
    .getOrCreate()
    
  import spark.implicits._ 
  //create a CaseClass for strongly typed dataset records
  case class Person(name: String, age:Long)
  val exampleDS = Seq(Person("MoGo", 42l)).toDS
  exampleDS.show()
  
  //Encoders for most common type are provide by spark.implicits._
  val primitiveDS = Seq(1,2,3).toDS()
  primitiveDS.show()
  primitiveDS.map(_ + 1).collect()
  
  val path = "src/main/resources/people.json"
  val peopleDS = spark.read.json(path).as[Person]
  peopleDS.show()
  
 
  
  spark.stop()
  
}